package test

import (
	"encoding/base64"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/compression"
	"testing"
)

var compressedBase64String = "eJw1y8EKAiEAhOF3mbOQq2u1HoPo1KVrhYh6EHRd1C0oevdM6Pp/M2+YFKOeLSROrp59MRf31NmCILuqTLIOkhLEJip3UsGXCnm9E9SiHjpADoyKiYsd5621srbTIEbKOB+307+FZPr4B3TPOrzS3LY4rjkt7rY5uBz8jM8XvLQwEA=="

var plainDecompressedString = "{\"command\":\"GetMiscReward\",\"ret_code\":0,\"misc_reward_list\":[],\"ts_val\":1205935733,\"tvalue\":1540233469,\"tvaluelocal\":1540208269,\"tzone\":\"Europe/Berlin\"}"

func TestCanCompressData(t *testing.T) {
	swCompression := compression.NewSwCompression()

	compressedBytes, err := swCompression.CompressBytes([]byte(plainDecompressedString))
	if err != nil {
		panic(err)
	}

	encodedCompressedString := base64.StdEncoding.EncodeToString(compressedBytes)
	if compressedBase64String != encodedCompressedString {
		logrus.Errorf("%x", compressedBytes)
		logrus.Error(encodedCompressedString)
		logrus.Error(compressedBase64String)
	}
}

func TestCanDecompressData(t *testing.T) {
	swCompression := compression.NewSwCompression()

	compressedBytes, err := swCompression.CompressBytes([]byte(plainDecompressedString))
	if err != nil {
		logrus.Panic(err)
	}

	encodedCompressedString := base64.StdEncoding.EncodeToString(compressedBytes)

	if encodedCompressedString != compressedBase64String {
		logrus.Errorf("%x", compressedBytes)
		logrus.Error(encodedCompressedString)
		logrus.Error(compressedBase64String)
	}
}
