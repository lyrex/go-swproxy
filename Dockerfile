FROM golang:alpine as build
RUN apk add --no-cache ca-certificates git
WORKDIR /go/src/gitlab.com/lyrex/go-swproxy
ADD . .
RUN CGO_ENABLED=0 GOOS=linux \
    go build -ldflags '-extldflags "-static"' -o go-swproxy cmd/proxy/main.go


FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt \
     /etc/ssl/certs/ca-certificates.crt
COPY --from=build /go/src/gitlab.com/lyrex/go-swproxy/go-swproxy /go-swproxy
ENTRYPOINT ["/go-swproxy"]