module gitlab.com/lyrex/go-swproxy

go 1.14

require (
	github.com/elazarl/goproxy v0.0.0-20180725130230-947c36da3153
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/olebedev/emitter v0.0.0-20190110104742-e8d1457e6aee
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	gopkg.in/resty.v1 v1.12.0
)
