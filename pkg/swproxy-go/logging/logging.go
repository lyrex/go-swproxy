package logging

import "github.com/sirupsen/logrus"

func NewApp(module string) *logrus.Entry {
	return logrus.StandardLogger().WithFields(logrus.Fields{
		"log_type": "app",
		"module":   module,
	})
}
func NewPlugin(module string) *logrus.Entry {
	return logrus.StandardLogger().WithFields(logrus.Fields{
		"log_type": "plugin",
		"module":   module,
	})
}


func NewAccess(module string) *logrus.Entry {
	return logrus.StandardLogger().WithFields(logrus.Fields{
		"log_type": "access",
		"module":   module,
	})
}
