package config

type AppConfig struct {
	Host           string `default:"" envconfig:"host"`
	Port           uint16 `default:"8081" envconfig:"port"`
	SwarfarmApiKey string `default:"" envconfig:"swarfarm_api_key"`

	SwarfarmApiKeys map[string]string `envconfig:"swarfarm_api_keys"`
}
