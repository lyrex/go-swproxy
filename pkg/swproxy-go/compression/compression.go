package compression

type Compression interface {
	CompressBytes(plaintext []byte) ([]byte, error)
	CompressString(plaintext string) (string, error)

	DecompressBytes(compressed []byte) ([]byte, error)
	DecompressString(compressed string) (string, error)
}