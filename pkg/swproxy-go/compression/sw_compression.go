package compression

import (
	"bytes"
	"compress/zlib"
	_ "compress/zlib"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
	"io/ioutil"
)

type swCompression struct {
	log logrus.FieldLogger
}

func (c *swCompression) CompressBytes(plaintext []byte) ([]byte, error) {
	var buf bytes.Buffer

	w := zlib.NewWriter(&buf)
	_, err := w.Write([]byte(plaintext))
	if err != nil {
		panic(err)
	}
	err = w.Flush()
	if err != nil {
		panic(err)
	}

	err = w.Close()
	if err != nil{
		panic(err)
	}

	return buf.Bytes(), nil
}

func (c *swCompression) CompressString(plaintext string) (string, error) {
	compressed, err := c.CompressBytes([]byte(plaintext))

	return string(compressed[:]), err
}

func (c *swCompression) DecompressBytes(compressed []byte) ([]byte, error) {
	r, err := zlib.NewReader(bytes.NewReader(compressed))
	if err != nil {
		panic(err)
	}

	decompressed, err := ioutil.ReadAll(r)
	if err != nil {
		panic(err)
	}

	return decompressed, nil
}

func (c *swCompression) DecompressString(compressed string) (string, error) {
	decompressedBytes, err := c.DecompressBytes([]byte(compressed))

	return string(decompressedBytes[:]), err
}

func NewSwCompression() *swCompression {
	logger := logging.NewApp("SwCompression")

	return &swCompression{
		log: logger,
	}
}

