package events

import (
	"github.com/olebedev/emitter"
)

type GameEventEmitter = emitter.Emitter

type GameEventType int

const (
	GameRequest GameEventType = iota
	GameResponse
)

func (e GameEventType) String() string {
	switch e {
	case GameRequest:
		return "GameRequest"
	case GameResponse:
		return "GameResponse"
	default:
		return "undefined"
	}
}

var GameEvents *GameEventEmitter

func init() {
	GameEvents = &GameEventEmitter{}
}
