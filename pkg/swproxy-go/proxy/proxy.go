package proxy

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"github.com/elazarl/goproxy"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/compression"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/crypto"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/events"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
	"io/ioutil"
	"net/http"
)

type serverMessage struct {
	Command string `json:"command"`
	TSValue int64  `json:"ts_val"`
}

type proxy struct {
	log logrus.FieldLogger

	crypto      crypto.Crypto
	compression compression.Compression
}

func New() *proxy {
	proxy := proxy{
		log: logging.NewApp("Proxy"),

		crypto:      crypto.NewSwCrypto(),
		compression: compression.NewSwCompression(),
	}

	return &proxy
}

func (p *proxy) CreateProxy() http.Handler {
	proxy := goproxy.NewProxyHttpServer()

	err := setCA([]byte(caCert), []byte(caKey))
	if err != nil {
		p.log.WithError(err).Panic("could not set proxy CA")
	}

	proxy.OnRequest(NewProxyGameEndpointMatcher()).HandleConnect(goproxy.AlwaysMitm)

	proxy.OnRequest(NewGameEndpointMatcher()).
		DoFunc(p.onRequest)

	proxy.OnResponse(NewGameEndpointMatcher()).
		DoFunc(p.onResponse)

	return proxy
}

func (p *proxy) onRequest(req *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
	p.log.WithFields(logrus.Fields{
		"ctx.Req.URL":    ctx.Req.URL,
		"ctx.Req.Header": ctx.Req.Header,
		"ctx.Session":    ctx.Session,
	}).Trace("New outgoing request")

	if req == nil || req.ContentLength == 0 {
		p.log.WithFields(logrus.Fields{
			"ctx.Session": ctx.Session,
		}).Info("Sending empty request to API")
	} else {
		if req.Body != nil {
			oldBody := req.Body
			defer oldBody.Close()

			reqBody, err := ioutil.ReadAll(req.Body)
			if err != nil {
				p.log.WithError(err).Panic("could not read request body")
			}
			req.Body = ioutil.NopCloser(bytes.NewBuffer(reqBody))

			reqContent := string(reqBody[:])
			plainContent := p.readBody(reqContent, false)

			ctx.UserData = plainContent

			p.log.WithFields(logrus.Fields{
				"ctx.Session": ctx.Session,
				"state":       "encrypted",
				"content":     reqContent,
			}).Trace("Sending request to API (encrypted)")

			var parsedContent serverMessage
			err = json.Unmarshal([]byte(plainContent), &parsedContent)
			if err != nil {
				p.log.WithError(err).Panic(err)
			}

			go events.GameEvents.Emit(parsedContent.Command, int(events.GameRequest), plainContent)

			p.log.WithFields(logrus.Fields{
				"ctx.Session": ctx.Session,
				"state":       "decrypted",
				"content":     plainContent,
			}).Debug("Sending request to API (decrypted)")
		}
	}

	return req, nil
}

func (p *proxy) onResponse(resp *http.Response, ctx *goproxy.ProxyCtx) *http.Response {
	p.log.WithFields(logrus.Fields{
		"ctx.Req.URL":    ctx.Req.URL,
		"ctx.Req.Header": ctx.Req.Header,

		"ctx.Session": ctx.Session,

		// "resp.Header": resp.Header,
	}).Trace("New incoming response")

	if resp == nil || resp.ContentLength == 0 {
		p.log.WithFields(logrus.Fields{
			"ctx.Session": ctx.Session,
		}).Info("Received empty reponse from API")
	} else {
		if resp.Body != nil {
			oldBody := resp.Body
			defer oldBody.Close()

			respBody, err := ioutil.ReadAll(resp.Body)
			respContent := string(respBody[:])

			plainContent := p.readBody(respContent, true)

			resp.Body = ioutil.NopCloser(bytes.NewBuffer(respBody))

			p.log.WithFields(logrus.Fields{
				"ctx.Session": ctx.Session,
				"content":     respContent,
			}).Trace("Receiving response from API (encrypted)")

			var parsedContent serverMessage
			err = json.Unmarshal([]byte(plainContent), &parsedContent)
			if err != nil {
				panic(err)
			}

			go events.GameEvents.Emit(parsedContent.Command, int(events.GameResponse), plainContent)

			requestPlainContent := ctx.UserData.(string)
			go events.ApiEvents.Emit(parsedContent.Command, events.ApiEventMsg{
				Request:  requestPlainContent,
				Response: plainContent,
			})

			p.log.WithFields(logrus.Fields{
				"ctx.Session": ctx.Session,
				"state":       "decrypted",
				"content":     plainContent,
			}).Debug("Receiving response from API (decrypted, decompressed)")
		}
	}

	return resp
}

func (p *proxy) readBody(body string, decompress bool) string {
	if len(body) == 0 {
		return ""
	}

	encryptedBody := body

	encryptedBytes, err := base64.StdEncoding.DecodeString(encryptedBody)
	if err != nil {
		p.log.WithError(err).Panic("could not decode content")
	}

	decryptedBytes, err := p.crypto.DecryptBytes(encryptedBytes)
	if err != nil {
		p.log.WithError(err).Panic("could not decrypt data")
	}

	// we're done if we don't need to decompress any data
	if !decompress {
		return string(decryptedBytes[:])
	}

	// otherwise decompress and return decompressed data
	decompressedBytes, err := p.compression.DecompressBytes(decryptedBytes)
	if err != nil {
		p.log.WithError(err).Panic("could not decompress data")
	}

	return string(decompressedBytes[:])
}
