package proxy

import (
	"github.com/elazarl/goproxy"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
	"net/http"
	"strings"
)

//// Proxy Game Endpoint Matcher
// used to determine if there's a CONNECT request to the Com2uS game server
type proxyGameEndpointMatcher struct {
	Log logrus.FieldLogger
}

func NewProxyGameEndpointMatcher() *proxyGameEndpointMatcher {
	log := logging.NewApp("ProxyGameEndpointMatcher")

	return &proxyGameEndpointMatcher{
		Log: log,
	}
}

func (s proxyGameEndpointMatcher) HandleReq(req *http.Request, ctx *goproxy.ProxyCtx) bool {
	return s.matches(ctx)
}

func (s proxyGameEndpointMatcher) HandleResp(resp *http.Response, ctx *goproxy.ProxyCtx) bool {
	return s.matches(ctx)
}

func (s proxyGameEndpointMatcher) matches(ctx *goproxy.ProxyCtx) bool {
	methodMatches := ctx.Req.Method == "CONNECT"

	hostMatches := strings.HasPrefix(ctx.Req.Host, "summonerswar-") &&
		strings.HasSuffix(ctx.Req.Host, "qpyou.cn")

	if hostMatches {
		s.Log.WithFields(logrus.Fields{
			"host":        ctx.Req.Host,
			"url":         ctx.Req.URL,
			"method":      ctx.Req.Method,
			"isGetMethod": methodMatches,
			"hostMatches": hostMatches,
		}).Trace()
	}

	return methodMatches && hostMatches
}

//// Game Endpoint Matcher
// used to intercept requests from and to the Com2uS game server
type gameEndpointMatcher struct {
	Log logrus.FieldLogger
}

func NewGameEndpointMatcher() *gameEndpointMatcher {
	log := logging.NewApp("GameEndpointMatcher")

	return &gameEndpointMatcher{
		Log: log,
	}
}

func (s gameEndpointMatcher) HandleReq(req *http.Request, ctx *goproxy.ProxyCtx) bool {
	return s.matches(ctx)
}

func (s gameEndpointMatcher) HandleResp(resp *http.Response, ctx *goproxy.ProxyCtx) bool {
	return s.matches(ctx)
}

func (s gameEndpointMatcher) matches(ctx *goproxy.ProxyCtx) bool {
	methodMatches := ctx.Req.Method == "GET" || ctx.Req.Method == "POST"
	hostMatches := strings.Contains(ctx.Req.Host, "qpyou.cn")
	urlMatches := ctx.Req.URL.Path == "/api/gateway_c2.php"

	s.Log.WithFields(logrus.Fields{
		"host":        ctx.Req.Host,
		"url":         ctx.Req.URL,
		"method":      ctx.Req.Method,
		"isGetMethod": methodMatches,
		"hostMatches": hostMatches,
		"urlMatches":  urlMatches,
	}).Trace()

	return methodMatches && hostMatches && urlMatches
}
