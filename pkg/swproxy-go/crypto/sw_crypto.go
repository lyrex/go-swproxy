package crypto

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"errors"

	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
)

type swCrypto struct {
	key []byte
	iv  []byte

	cipher cipher.Block

	log logrus.FieldLogger
}

func NewSwCrypto() *swCrypto {
	logger := logging.NewApp("SwCrypto")

	key := []byte("Gr4S2eiNl7zq5MrU")
	iv := make([]byte, 16)

	c, err := aes.NewCipher(key)
	if err != nil {
		logger.Fatal(err)
	}

	return &swCrypto{
		key: key[:aes.BlockSize],
		iv:  iv[:aes.BlockSize],

		cipher: c,

		log: logger,
	}
}

func (c *swCrypto) EncryptBytes(plaintext []byte) ([]byte, error) {
	paddedPlaintext, err := pkcs7Pad(plaintext, aes.BlockSize)
	if err != nil {
		panic(err)
	}

	encrypter := cipher.NewCBCEncrypter(c.cipher, c.iv)

	ciphertext := make([]byte, len(paddedPlaintext))
	encrypter.CryptBlocks(ciphertext, paddedPlaintext)

	return ciphertext, nil
}

func (c *swCrypto) EncryptString(plaintext string) (string, error) {
	ciphertext, err := c.EncryptBytes([]byte(plaintext))

	return string(ciphertext[:]), err
}

func (c *swCrypto) DecryptBytes(ciphertext []byte) ([]byte, error) {
	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		c.log.Error("ciphertext too short")
		return []byte{}, errors.New("ciphertext too short")
	}

	// CBC mode always works in whole blocks.
	if len(ciphertext)%aes.BlockSize != 0 {
		c.log.Error("ciphertext is not a multiple of the block size")
		return []byte{}, errors.New("ciphertext is not a multiple of the block size")
	}

	decrypter := cipher.NewCBCDecrypter(c.cipher, c.iv)

	cipherBytes := make([]byte, len(ciphertext))
	copy(cipherBytes, ciphertext)

	decrypter.CryptBlocks(cipherBytes, cipherBytes)
	decryptedBytes := cipherBytes[:]

	return pkcs7Unpad(decryptedBytes, aes.BlockSize)
}

func (c *swCrypto) DecryptString(ciphertext string) (string, error) {
	decryptedBytes, err := c.DecryptBytes([]byte(ciphertext))

	return string(decryptedBytes[:]), err
}

// PKCS7 errors.
var (
	// ErrInvalidBlockSize indicates hash blocksize <= 0.
	ErrInvalidBlockSize = errors.New("invalid blocksize")

	// ErrInvalidPKCS7Data indicates bad input to PKCS7 pad or unpad.
	ErrInvalidPKCS7Data = errors.New("invalid PKCS7 data (empty or not padded)")

	// ErrInvalidPKCS7Padding indicates PKCS7 unpad fails to bad input.
	ErrInvalidPKCS7Padding = errors.New("invalid padding on input")
)

// pkcs7Pad right-pads the given byte slice with 1 to n bytes, where
// n is the block size. The size of the result is x times n, where x
// is at least 1.
func pkcs7Pad(b []byte, blocksize int) ([]byte, error) {
	if blocksize <= 0 {
		return nil, ErrInvalidBlockSize
	}
	if b == nil || len(b) == 0 {
		return nil, ErrInvalidPKCS7Data
	}
	n := blocksize - (len(b) % blocksize)
	pb := make([]byte, len(b)+n)
	copy(pb, b)
	copy(pb[len(b):], bytes.Repeat([]byte{byte(n)}, n))
	return pb, nil
}

func pkcs7Unpad(b []byte, blocksize int) ([]byte, error) {
	if blocksize <= 0 {
		return nil, ErrInvalidBlockSize
	}
	if b == nil || len(b) == 0 {
		return nil, ErrInvalidPKCS7Data
	}
	if len(b)%blocksize != 0 {
		return nil, ErrInvalidPKCS7Padding
	}
	c := b[len(b)-1]
	n := int(c)
	if n == 0 || n > len(b) {
		return nil, ErrInvalidPKCS7Padding
	}

	return b[:len(b)-n], nil
}
