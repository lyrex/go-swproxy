package crypto

type Crypto interface {
	EncryptBytes(plaintext []byte) ([]byte, error)
	EncryptString(plaintext string) (string, error)

	DecryptBytes(ciphertext []byte) ([]byte, error)
	DecryptString(ciphertext string) (string, error)
}