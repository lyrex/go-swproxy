package profileexport

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sort"

	"github.com/olebedev/emitter"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/events"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
)

type profileExport struct {
	name        string
	description string

	log          logrus.FieldLogger
	activeEvents *emitter.Group
	isLoaded     bool
}

func NewProfileExportPlugin() *profileExport {
	name := "ProfileExport"
	desc := "Export all monster and rune data to a json file."

	p := profileExport{
		name:        name,
		description: desc,

		log:          logging.NewPlugin(name),
		activeEvents: &emitter.Group{Cap: 1},
		isLoaded:     false,
	}

	return &p
}

func (p *profileExport) GetName() string {
	return p.name
}

func (p *profileExport) GetDescription() string {
	return p.description
}

func (p *profileExport) IsLoaded() bool {
	return p.isLoaded
}

func (p *profileExport) Load() {
	p.activeEvents.Add(events.ApiEvents.On("HubUserLogin"), events.ApiEvents.On("GuestLogin"))

	go p.eventLoop()
	p.isLoaded = true
}

func (p *profileExport) Unload() {
	p.isLoaded = false
	p.activeEvents.Flush()
}

func (p *profileExport) eventLoop() {
	for event := range p.activeEvents.On() {
		apiEvent := event.Args[0].(events.ApiEventMsg)
		apiResponse := apiEvent.Response

		content := map[string]interface{}{}
		err := json.Unmarshal([]byte(apiResponse), &content)
		if err != nil {
			p.log.WithError(err).Error()
			continue
		}
		command := content["command"]
		wizardInfo := content["wizard_info"].(map[string]interface{})

		wizardId := uint64(wizardInfo["wizard_id"].(float64))
		wizardName := wizardInfo["wizard_name"]

		if _, err := os.Stat("export"); os.IsNotExist(err) {
			err = os.Mkdir("export", 0755)
			if err != nil {
				p.log.WithError(err).Error("Could not create profile export directory")
				continue
			}
		}

		p.log.WithFields(logrus.Fields{"command": command, "wizard_id": wizardId}).
			Info("Received command used in profile export")

		// check data integrity
		dataIsOk := p.checkData(content)
		if !dataIsOk {
			p.log.Error("Some data in the API response is missing. Please retry.")
			continue
		}

		// sort data
		sortedData := p.sortData(content)

		// serialize sorted data back to json
		jsonBytes, err := json.Marshal(sortedData)
		if err != nil {
			p.log.WithError(err).Error("Error on command deserialization")
			continue
		}

		// write sorted data to profile file
		filePath := fmt.Sprintf("export/%v-%v.json", wizardName, wizardId)
		err = ioutil.WriteFile(filePath, jsonBytes, 0664)
		if err != nil {
			p.log.WithFields(logrus.Fields{"wizard_id": wizardId}).
				WithError(err).Error("Could not write profile JSON file")
			continue
		}

		p.log.WithFields(logrus.Fields{"wizard_id": wizardId}).
			Info(fmt.Sprintf("Profile successfully exported to %s", filePath))
	}
}

func (p *profileExport) checkData(data map[string]interface{}) bool {
	_, hasBuildingList := data["building_list"]

	return hasBuildingList
}

func (p *profileExport) sortData(data map[string]interface{}) map[string]interface{} {
	// find storage building
	var storageId uint64 = 999
	buildingList := data["building_list"].([]interface{})

	for _, entry := range buildingList {
		building := entry.(map[string]interface{})

		buildingMasterId := uint64(building["building_master_id"].(float64))
		buildingId := uint64(building["building_id"].(float64))

		if buildingMasterId == 25 {
			storageId = buildingId
		}
	}

	// sort unit list
	unitListRef := data["unit_list"].([]interface{})
	sort.Slice(unitListRef, func(i, j int) bool {
		a := newJsonUnit(unitListRef[i].(map[string]interface{}))
		b := newJsonUnit(unitListRef[j].(map[string]interface{}))

		if a.BuildingId == storageId || b.BuildingId == storageId {
			aIsStorage := a.BuildingId == storageId
			bIsStorage := b.BuildingId == storageId
			if aIsStorage && !bIsStorage {
				return true
			} else if !aIsStorage && bIsStorage {
				return false
			}
		}

		if Abs(int64(b.Class-a.Class)) != 0 {
			return a.Class < b.Class
		}

		if Abs(int64(b.UnitLevel-a.UnitLevel)) != 0 {
			return a.UnitLevel < b.UnitLevel
		}

		if Abs(int64(a.Attribute-b.Attribute)) != 0 {
			return a.Attribute > b.Attribute
		}

		if Abs(int64(a.UnitId-b.UnitId)) != 0 {
			return a.UnitId > b.UnitId
		}

		return false
	})

	// reverse sorting
	for i := len(unitListRef)/2 - 1; i >= 0; i-- {
		opp := len(unitListRef) - 1 - i
		unitListRef[i], unitListRef[opp] = unitListRef[opp], unitListRef[i]
	}

	// sort runes on monsters by slot
	for _, entry := range unitListRef {
		unit := entry.(map[string]interface{})

		sortedUnitRunes := sortRunes(unit["runes"])
		unit["runes"] = sortedUnitRunes
	}

	// sort runes in inventory by slot
	sortedRunes := sortRunes(data["runes"])
	data["runes"] = sortedRunes

	// sort craft items
	craftItems := data["rune_craft_item_list"].([]interface{})
	sort.Sort(CraftItemsByTypeAndId(craftItems))

	return data
}

func sortRunes(runeElement interface{}) interface{} {
	runes, ok := runeElement.([]interface{})
	if !ok {
		runes = make([]interface{}, 0)
		for _, v := range runeElement.(map[string]interface{}) {
			runes = append(runes, v)
		}
	}
	sort.Sort(RunesBySlot(runes))
	return runes
}

func newJsonUnit(unitListEntry map[string]interface{}) jsonUnit {
	return jsonUnit{
		UnitId:     uint64(unitListEntry["unit_id"].(float64)),
		BuildingId: uint64(unitListEntry["building_id"].(float64)),
		UnitLevel:  uint(unitListEntry["unit_level"].(float64)),
		Class:      uint(unitListEntry["class"].(float64)),
		Attribute:  uint(unitListEntry["attribute"].(float64)),
	}
}

type jsonUnit struct {
	UnitId     uint64 `json:"unit_id"`
	BuildingId uint64 `json:"building_id"`
	UnitLevel  uint   `json:"unit_level"`
	Class      uint   `json:"class"`
	Attribute  uint   `json:"attribute"`
}

func Abs(x int64) int64 {
	if x < 0 {
		return -x
	}
	return x
}

// Sort units according to the rank etc.
type CraftItemsByTypeAndId []interface{}

func (r CraftItemsByTypeAndId) Len() int { return len(r) }

func (r CraftItemsByTypeAndId) Less(i, j int) bool {
	a := r[i].(map[string]interface{})
	b := r[j].(map[string]interface{})

	typeA := uint(a["craft_type"].(float64))
	typeB := uint(b["craft_type"].(float64))

	itemIdA := uint(a["craft_item_id"].(float64))
	itemIdB := uint(b["craft_item_id"].(float64))

	if Abs(int64(typeB-typeA)) != 0 {
		return typeA < typeB
	}

	if Abs(int64(itemIdB-itemIdA)) != 0 {
		return itemIdA > itemIdB
	}

	return false
}

func (r CraftItemsByTypeAndId) Swap(i, j int) { r[i], r[j] = r[j], r[i] }

// Sort interface for runes
type RunesBySlot []interface{}

func (r RunesBySlot) Len() int { return len(r) }

func (r RunesBySlot) Less(i, j int) bool {
	runeA := r[i].(map[string]interface{})
	runeB := r[j].(map[string]interface{})

	slotA := uint(runeA["slot_no"].(float64))
	slotB := uint(runeB["slot_no"].(float64))

	return slotA < slotB
}

func (r RunesBySlot) Swap(i, j int) { r[i], r[j] = r[j], r[i] }
