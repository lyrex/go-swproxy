package dbghelp

import (
	"encoding/json"
	"github.com/olebedev/emitter"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/events"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
)

type dbgHelp struct {
	name        string
	description string

	log          logrus.FieldLogger
	activeEvents *emitter.Group
}

func NewDbgHelpPlugin() *dbgHelp {
	name := "Debug Helper"
	desc := "Plugin that is made to support while debugging."

	p := dbgHelp{
		name:        name,
		description: desc,

		log:          logging.NewPlugin(name),
		activeEvents: &emitter.Group{Cap: 1},
	}

	return &p
}

func (p *dbgHelp) GetName() string {
	return p.name
}

func (p *dbgHelp) GetDescription() string {
	return p.description
}

func (p *dbgHelp) Load() {
	p.activeEvents.Add(events.GameEvents.On("*"))

	go p.eventLoop()
}

func (p *dbgHelp) Unload() {
	p.activeEvents.Flush()
}

func (p *dbgHelp) eventLoop() {
	for event := range p.activeEvents.On() {
		eventType := events.GameEventType(event.Int(0))
		data := event.String(1)

		content := map[string]interface{}{}
		err := json.Unmarshal([]byte(data), &content)
		if err != nil {
			p.log.WithError(err).Error(err)
			continue
		}

		p.log.WithFields(logrus.Fields{
			"eventType": eventType,
			"command": content["command"],
		}).Info("Debug plugin")
		p.log.WithFields(logrus.Fields(content)).Trace("Debug plugin data")
	}
}
