package pluginbase

type Plugin interface {
	GetName() string
	GetDescription() string

	IsLoaded() bool

	Load()
	Unload()
}
