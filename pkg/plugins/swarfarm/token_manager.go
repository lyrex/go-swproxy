package swarfarm

import "errors"

var apiTokenMap map[string]string

func AddProfile(summonerId string, token string) {
	apiTokenMap[summonerId] = token
}

func FindToken(summonerId string) (string, error) {
	token, ok := apiTokenMap[summonerId]

	if !ok {
		return "", errors.New("no associated token found")
	}

	return token, nil
}

func init() {
	apiTokenMap = make(map[string]string)
}