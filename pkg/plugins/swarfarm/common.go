package swarfarm

import (
	"fmt"
	"gopkg.in/resty.v1"
)

const (
	baseUrl = "https://swarfarm.com"
	apiUrl  = baseUrl + "/api/v2"
)

func makeAuthorizedRequest(apiToken string) *resty.Request {
	req := resty.R()

	if apiToken != "" {
		req.SetHeader("Authorization", fmt.Sprintf("Token %s", apiToken))
	}

	return req
}
