package swarfarm

import (
	"encoding/json"
	"github.com/olebedev/emitter"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/events"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
	"gopkg.in/resty.v1"
	"strconv"
	"strings"
)

type swarfarmLogger struct {
	name        string
	description string

	log          logrus.FieldLogger
	activeEvents *emitter.Group
	isLoaded     bool
}

func NewSwarfarmLoggerPlugin() *swarfarmLogger {
	name := "SWARFARM Logger"
	desc := "Plugin that integrates into the public SWARFARM API."

	p := swarfarmLogger{
		name:        name,
		description: desc,

		log:          logging.NewPlugin(name),
		activeEvents: &emitter.Group{Cap: 1},
		isLoaded:     false,
	}

	return &p
}

func (p *swarfarmLogger) GetName() string {
	return p.name
}

func (p *swarfarmLogger) GetDescription() string {
	return p.description
}

func (p *swarfarmLogger) IsLoaded() bool {
	return p.isLoaded
}

func (p *swarfarmLogger) Load() {
	commands := p.getAcceptedCommands()
	listenEvents := make([]<-chan emitter.Event, 0)

	keys := make([]string, 0, len(commands))
	for k := range commands {
		keys = append(keys, k)
	}

	for key := range commands {
		listenEvents = append(listenEvents, events.ApiEvents.On(key))
	}

	p.activeEvents.Add(listenEvents...)

	go p.eventLoop()
	p.isLoaded = true
}

func (p *swarfarmLogger) Unload() {
	p.isLoaded = false
	p.activeEvents.Flush()
}

func (p *swarfarmLogger) eventLoop() {
	acceptedCommands := p.getAcceptedCommands()

	for event := range p.activeEvents.On() {
		apiEvent := event.Args[0].(events.ApiEventMsg)

		request := map[string]interface{}{}
		err := json.Unmarshal([]byte(apiEvent.Request), &request)
		if err != nil {
			p.log.WithError(err).Error("Error while deserializing request")
			continue
		}

		response := map[string]interface{}{}
		err = json.Unmarshal([]byte(apiEvent.Response), &response)
		if err != nil {
			p.log.WithError(err).Error("Error while deserializing response")
			continue
		}

		wizardId := int(request["wizard_id"].(float64))
		apiToken, _ := FindToken(strconv.Itoa(wizardId))

		inputMap := make(map[string]map[string]interface{})
		inputMap["request"] = request
		inputMap["response"] = response

		p.log.WithFields(logrus.Fields{"command": request["command"], "wizard_id": wizardId}).Debug("Uploading command data to SWARFARM")

		swarfarmCommand := make(map[string]map[string]interface{})
		cmdGroup := acceptedCommands[request["command"].(string)]
		for direction := range cmdGroup {
			swarfarmCommand[direction] = make(map[string]interface{})
		}

		// handle request fields
		categories := []string{"request", "response"}

		for _, cat := range categories {
			requestCmds, ok := cmdGroup[cat]
			if !ok {
				continue
			}

			for _, c := range requestCmds {
				e, ok := inputMap[cat][c]

				if ok {
					swarfarmCommand[cat][c] = e
				} else {
					swarfarmCommand[cat][c] = nil
				}
			}
		}

		// handle response fields

		swarfarmCommandContent := make(map[string]interface{})
		swarfarmCommandContent["data"] = swarfarmCommand

		jsonBytes, err := json.Marshal(swarfarmCommandContent)
		if err != nil {
			p.log.WithError(err).Error("Error on command deserialization")
			continue
		}

		jsonString := string(jsonBytes[:])
		resp, err := makeAuthorizedRequest(apiToken).
			SetHeader("Content-Type", "application/json").
			SetBody(jsonString).
			Post(baseUrl + "/data/log/upload/")

		if err != nil {
			p.log.WithField("wizard_id", wizardId).WithError(err).Error("SWARFARM upload failed")
			continue
		}

		if resp.StatusCode() != 200 {
			p.log.WithFields(logrus.Fields{"StatusCode": resp.StatusCode(), "wizard_id": wizardId}).Error("SWARFARM upload failed. Invalid status code.")
			continue
		}

		p.log.WithFields(logrus.Fields{"command": request["command"], "wizard_id": wizardId}).Info("Upload successful")
	}
}

func (p *swarfarmLogger) getAcceptedCommands() map[string]map[string][]string {
	acceptedCommands := make(map[string]map[string][]string, 0)

	p.log.Debug("Retrieving list of accepted log types from SWARFARM...")

	resp, err := resty.R().Get(baseUrl + "/data/log/accepted_commands/")
	if err != nil {
		p.log.WithError(err).Error("Unable to retrieve accepted log types. SWARFARM logging is disabled.")
		return acceptedCommands
	}

	if resp.StatusCode() != 200 {
		p.log.WithError(err).Error("Unable to retrieve accepted log types. Invalid status code. SWARFARM logging is disabled.")
		return acceptedCommands
	}

	content := map[string]interface{}{}
	err = json.Unmarshal(resp.Body(), &content)
	if err != nil {
		p.log.WithError(err).Error("Error while deserializing accepted commands")
		return acceptedCommands
	}

	for k, v := range content {
		// skip non-commands
		if strings.HasPrefix(k, "__") {
			continue
		}

		contentCmd := v.(map[string]interface{})

		cmd := make(map[string][]string, 1)
		for cmdDirection, validValues := range contentCmd {
			for _, validValue := range validValues.([]interface{}) {
				cmd[cmdDirection] = append(cmd[cmdDirection], validValue.(string))
			}
		}

		acceptedCommands[k] = cmd
	}

	keys := make([]string, 0, len(acceptedCommands))
	for k := range acceptedCommands {
		keys = append(keys, k)
	}
	p.log.WithField("accepted_commands", keys).Info("Successfully retrieded list of SWARFARM accepted commands")

	return acceptedCommands
}
