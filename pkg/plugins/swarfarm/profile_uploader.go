package swarfarm

import (
	"encoding/json"
	"fmt"
	"github.com/olebedev/emitter"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/events"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
	"net/http"
	"strconv"
	"time"
)

type swarfarmProfileUploader struct {
	name        string
	description string

	log          logrus.FieldLogger
	activeEvents *emitter.Group
	isLoaded     bool
}

func NewSwarfarmProfileUploader() *swarfarmProfileUploader {
	name := "SWARFARM Profile Uploader"
	desc := "Plugin that automatically uploads your SWARFARM profile."

	p := swarfarmProfileUploader{
		name:        name,
		description: desc,

		log:          logging.NewPlugin(name),
		activeEvents: &emitter.Group{Cap: 1},
		isLoaded:     false,
	}

	return &p
}

func (p *swarfarmProfileUploader) GetName() string {
	return p.name
}

func (p *swarfarmProfileUploader) GetDescription() string {
	return p.description
}

func (p *swarfarmProfileUploader) IsLoaded() bool {
	return p.isLoaded
}

func (p *swarfarmProfileUploader) Load() {
	p.activeEvents.Add(events.ApiEvents.On("HubUserLogin"))

	go p.eventLoop()
	p.isLoaded = true
}

func (p *swarfarmProfileUploader) Unload() {
	p.isLoaded = false
	p.activeEvents.Flush()
}

func (p *swarfarmProfileUploader) eventLoop() {
	for event := range p.activeEvents.On() {
		apiEvent := event.Args[0].(events.ApiEventMsg)
		apiResponse := apiEvent.Response

		content := map[string]interface{}{}
		err := json.Unmarshal([]byte(apiResponse), &content)
		if err != nil {
			p.log.WithError(err).Error("Error while deserializing SWARFARM response")
			continue
		}

		wizardInfo := content["wizard_info"].(map[string]interface{})
		wizardId := int(wizardInfo["wizard_id"].(float64))
		apiToken, _ := FindToken(strconv.Itoa(wizardId))

		p.log.WithField("wizard_id", wizardId).Info("Uploading profile to SWARFARM...")

		resp, err := makeAuthorizedRequest(apiToken).
			SetHeader("Content-Type", "application/json").
			SetBody(apiResponse).
			Post(apiUrl + "/profiles/upload/")

		if err != nil {
			p.log.WithError(err).Error("Failed to upload profile to SWARFARM.. Could not sent request")
			continue
		}

		content = map[string]interface{}{}
		err = json.Unmarshal(resp.Body(), &content)
		if err != nil {
			p.log.WithError(err).Error("Error while deserializing SWARFARM profile upload response")
			continue
		}

		switch resp.StatusCode() {
		case http.StatusOK:
			jobId := content["job_id"].(string)

			p.log.WithField("jobId", jobId).Info("SWARFARM profile successfully uploaded - awaiting import queue")

			// run job check routine
			go func(jobId string, apiKey string, pluginName string) {
				logger := logging.NewPlugin(pluginName)

				for retries := 0; retries < 10; retries++ {
					checkUrl := apiUrl + fmt.Sprintf("/profiles/upload/%s/", jobId)

					resp, err := makeAuthorizedRequest(apiKey).
						SetHeader("Content-Type", "application/json").
						Get(checkUrl)

					if err != nil {
						logger.WithError(err).Warn("Failed to check SWARFARM profile upload status..")
					}

					if resp.StatusCode() == http.StatusUnauthorized {
						logger.Error("Could not check for job, unauthorized")
						return
					} else if resp.StatusCode() == http.StatusOK {
						content := map[string]interface{}{}
						err = json.Unmarshal(resp.Body(), &content)
						if err != nil {
							logger.WithError(err).Error("Error while deserializing SWARFARM profile upload check response")
							return
						}

						if content["status"].(string) == "SUCCESS" {
							logger.Info("SWARFARM profile import complete!")
							return
						}
					}

					time.Sleep(10 * time.Second)
				}

				logger.Error("A timeout occured while waiting for the import status")
			}(jobId, apiToken, p.GetName())

			break
		case http.StatusBadRequest:
			p.log.WithField("body", resp.Body()).Error("Upload failed, invalid data provided")
			break
		case http.StatusUnauthorized:
			p.log.WithField("wizard_id", wizardId).Error("Unable to authorize. Please check your API key")
			break
		case http.StatusConflict:
			p.log.WithField("body", resp.Body()).Error("You need to upload your profile manually to resolve this")
			break
		default:
			p.log.WithField("resp", resp).Error("Received unknown response type")
			break
		}
	}
}
