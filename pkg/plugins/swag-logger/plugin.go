package swag_logger

import (
	"encoding/json"
	"github.com/olebedev/emitter"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/events"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
	"gopkg.in/resty.v1"
	"net/http"
)

type swagLogger struct {
	name        string
	description string

	log          logrus.FieldLogger
	activeEvents *emitter.Group
	isLoaded     bool
}

func NewSwagLogger() *swagLogger {
	name := "SWAG Logger"
	desc := "Automatically uploads guild war data to gw.swop.one"

	p := swagLogger{
		name:        name,
		description: desc,

		log:          logging.NewPlugin(name),
		activeEvents: &emitter.Group{Cap: 1},
		isLoaded:     false,
	}

	return &p
}

func (p *swagLogger) GetName() string {
	return p.name
}

func (p *swagLogger) GetDescription() string {
	return p.description
}

func (p *swagLogger) IsLoaded() bool {
	return p.isLoaded
}

func (p *swagLogger) Load() {
	p.activeEvents.Add(events.ApiEvents.On("GetGuildWarBattleLogByWizardId"), events.ApiEvents.On("GetGuildWarBattleLogByGuildId"))

	go p.eventLoop()
	p.isLoaded = true
}

func (p *swagLogger) Unload() {
	p.isLoaded = false
	p.activeEvents.Flush()
}

func (p *swagLogger) eventLoop() {
	for event := range p.activeEvents.On() {
		apiEvent := event.Args[0].(events.ApiEventMsg)
		apiResponse := apiEvent.Response

		requestContent := map[string]interface{}{}
		err := json.Unmarshal([]byte(apiResponse), &requestContent)
		if err != nil {
			p.log.WithError(err).Error("Error while deserializing SWAG response")
			continue
		}

		command := requestContent["command"]
		wizardId := requestContent["wizard_id"]

		p.log.WithField("wizard_id", wizardId).Info("Uploading guild war data to SWAG...")


		resp, err := resty.R().
			SetHeader("Content-Type", "application/json").
			SetBody(apiResponse).
			Post("https://gw.swop.one/data/upload/")

		if err != nil {
			p.log.WithError(err).Error("SWAG upload failed")
			continue
		}

		if resp.StatusCode() != http.StatusOK {
			p.log.WithFields(logrus.Fields{"StatusCode": resp.StatusCode(), "wizardId": wizardId}).
				Errorf("SWAG upload failed. Status %d", resp.StatusCode())
			continue
		}

		p.log.WithFields(logrus.Fields{"command": command, "wizardId": wizardId}).Info("SWAG upload successful.")
	}
}
