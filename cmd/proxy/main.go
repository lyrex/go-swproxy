package main

import (
	"context"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
	"gitlab.com/lyrex/go-swproxy/pkg/plugins/profileexport"
	swag_logger "gitlab.com/lyrex/go-swproxy/pkg/plugins/swag-logger"
	"gitlab.com/lyrex/go-swproxy/pkg/plugins/swarfarm"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/config"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/logging"
	"gitlab.com/lyrex/go-swproxy/pkg/swproxy-go/proxy"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{})

	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)

	logrus.Trace("init has been executed")
}

func loadConfig(log logrus.FieldLogger) config.AppConfig {
	var configuration config.AppConfig
	err := envconfig.Process("swproxy", &configuration)

	if err != nil {
		log.WithError(err).Panic("Could not load app configuration")
	}

	return configuration
}

func main() {
	log := logging.NewApp("main")
	appConfig := loadConfig(log)

	log.WithFields(logrus.Fields{
		"host": appConfig.Port,
		"port": appConfig.Port,
	}).Infof("Server listening to %s:%d", appConfig.Host, appConfig.Port)

	// TODO: Write something like a custom plugin-loader (hint: golang/plugins)
	// load and initialize plugins

	profileExportPlugin := profileexport.NewProfileExportPlugin()
	swarfarmLogger := swarfarm.NewSwarfarmLoggerPlugin()
	swarfarmProfileUploader := swarfarm.NewSwarfarmProfileUploader()
	swagLogger := swag_logger.NewSwagLogger()
	// debugPlugin := dbghelp.NewDbgHelpPlugin()

	profileExportPlugin.Load()
	swarfarmLogger.Load()
	swarfarmProfileUploader.Load()
	swagLogger.Load()
	//debugPlugin.Load()

	// setup SWARFARM token authentication
	for wizardId, token := range appConfig.SwarfarmApiKeys {
		log.Infof("Adding token for wizard %s", wizardId)

		swarfarm.AddProfile(wizardId, token)
	}

	swProxy := proxy.New()
	httpProxy := swProxy.CreateProxy()
	server := &http.Server{Addr: fmt.Sprintf("%s:%d", appConfig.Host, appConfig.Port), Handler: httpProxy}

	go func() {
		log := logging.NewApp("main")

		log.Info(server.ListenAndServe())
	}()

	// Setting up signal capturing
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	// Waiting for SIGINT (pkill -2)
	<-stop

	log.Info("Shutting down proxy...")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	if err := server.Shutdown(ctx); err != nil {
		log.WithError(err).Panic()
	}

	log.Info("Proxy shut down")
}
